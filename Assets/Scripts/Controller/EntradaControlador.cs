﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AF
{
    public class EntradaControlador : MonoBehaviour
    {
        float vertical;
        float horizontal;

        EstadoAdministrador estados;
        // Start is called before the first frame update
        void Start()
        {
            estados = GetComponent<EstadoAdministrador>();
            estados.Init();
        }

        private void FixedUpdate()
        {
            GetInput();
        }

        void GetInput()
        {
            vertical = Input.GetAxis("Vertical");
            horizontal = Input.GetAxis("Horizontal");
        }

        void UpdateStates()
        {
            estados.horiozontal = horizontal;
            estados.vertical = vertical;
            estados.Tick(Time.deltaTime);
        }
    }
}
